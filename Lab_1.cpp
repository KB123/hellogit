#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	vector <string> sentence;
	string str1 = "hello";
	string str2 = "how are you?";
	string str3 = "good";
	string str4 = str3 + ", great some would say";
	
	sentence.push_back("hi");
	sentence.push_back("good and you?");
	cout << str1 << endl;
	cout << sentence[0] << endl;
	cout << str2 << endl;
	cout << sentence[1] << endl;
	cout << str3 << ", " << str4 << endl;
	
	
}
